#!/usr/bin/python
# -*- coding=utf-8 -*-

import sys, os
sys.path.append(os.path.dirname(__file__))
import bookdb
from urlparse import parse_qs


def application(environ, start_response):

    url = parse_qs(environ.get('QUERY_STRING', 'Unset'))
    idksiazki = url.get('id', [''])

    exists = False

    for title in bookdb.BookDB().titles():
        if title['id'] == idksiazki[0]:
            exists = True

    response_body = """
    <html>
        <head>
            <meta charset="utf-8" />"""
    bookss = bookdb.BookDB()
    if exists:
        status = '200 OK'
        response_body+="""
                <title>Opis szczegółowy</title>
                </head>
                <body>"""
        response_body+= """<h2>Opis wybranej książki</h2><table>
            <tr><td><b>Tytuł: </b></td><td>%s</td></tr>
            <tr><td><b>ISBN: </b></td><td>%s</td></tr>
            <tr><td><b>Wydawca: </b></td><td>%s</td></tr>
            <tr><td><b>Autor: </b></td><td>%s</td></tr>
            </table>
            """ % (bookss.title_info(idksiazki[0])['title'], bookss.title_info(idksiazki[0])['isbn'],
                   bookss.title_info(idksiazki[0])['publisher'], bookss.title_info(idksiazki[0])['author'])
        response_body += """
            </br><a href="http://194.29.175.240/~p4/wsgi-bin/books.wsgi">Powrót do listy książek</a></body>
            </html>"""

    elif idksiazki[0]=='':
        status = '200 OK'
        response_body+="""
            <title>Lista książek</title>
            </head>
            <body><h2>Lista książek w bazie</h2><ul>"""
        for title in bookss.titles():
            response_body+='<li><a href="?id='+title['id']+'">'+title['title']+'</a></li></br>'
        response_body+= """
        </ul></body>
        </html>"""

    else:
        status = '404 Not Found'
        response_body+="""
        <title>Koniec internetów...</title>
        <head>
            <body>
                <h2>Koniec internetów...<h2>
                <img src="http://i1.ryjbuk.pl/148ed1fc5335b83f48f274463fa1698d14a337d2/na-koniec-internetuxx-jpg" alt="Tu był obrazek"/>
                <br/><br/><a href="http://194.29.175.240/~p4/wsgi-bin/books.wsgi">Powrót do listy książek</a>
            </body>
        </html>"""

    response_headers = [('Content-Type', 'text/html'),
                        ('Content-Length', str(len(response_body)))]
    start_response(status, response_headers)

    return [response_body]


if __name__ == '__main__':
    from wsgiref.simple_server import make_server
    #srv = make_server('localhost', 4464, application)
    srv = make_server('194.29.175.240', 31004, application)

    srv.serve_forever()